#!/usr/bin/env python

## Author: Jens Hooge
## Date: Aug 7, 2019
## Python Version 3.6
##
## Please see http://biobank.ndph.ox.ac.uk/showcase/exinfo.cgi?src=accessing_data_guide#download
## for information on UKB specific scripts

import os
import shutil
import sys
import argparse
import subprocess
import pandas as pd

from glob import glob

argparser = argparse.ArgumentParser()

argparser.add_argument("-d", "--data_field",
                       action="store", type=int,
                       help="Data-Field ID",
                       required=True)
argparser.add_argument("-e", "--encoding_file",
                       action="store", type=str,
                       help="Decrypted encoding file (ukbXXXX.enc_ukb) required to generate a list (bulkfile) of datafiles to be fetched.",
                       required=True)
# argparser.add_argument("-k", "--key_file",
#                    action="store", type=str,
#                    help="UKBiobank public key file (should include application ID and 64 char checksum).",
#                    required=True)
argparser.add_argument("-b", "--bulk_file",
                       action="store", type=str,
                       help="Path at which the bulk file should be saved.",
                       required=True)
argparser.add_argument("-o", "--output_dir",
                       action="store", type=str,
                       help="Output directory for downloaded files",
                       required=False)
argparser.add_argument(
    '--no_download',
    action='store_true',
    help='Download the bulk file only',
)

args = argparser.parse_args()
args = vars(args)


def check_env_vars():
    """Check whether environment variables have been set."""

    if ("UKBB_AUTH_KEY" not in os.environ):
        raise ValueError('Environment variable UKBB_AUTH_KEY not found!')

    if ("UKBB_SCRIPTS_DIR" not in os.environ):
        raise ValueError('Environment variable UKBB_SCRIPTS_DIR not found!')

    return (None)


def ensure_dir(file_path):
    """Check whether parent directory path for of file_path already exists
    and if not creates all directories on the parent path.

    Parameters:
    -----------
    file_path : str
        The location of the file, to which the parent directories should be created.
    """

    directory = os.path.dirname(file_path)
    if not os.path.exists(directory):
        os.makedirs(directory)

    return (None)


def clean_up():
    """Removes temporary files in current directory, which had been created during the
    downloading process.
    """

    fetched_files = glob("./*.lis")
    fields_file = "./fields.ukb"
    for f in fetched_files:
        os.remove(f)
    if os.path.isfile(fields_file):
        os.remove(fields_file)


def create_bulkfile(data_field, encoding_file, bulk_filename, log=True):
    """This function creates a UK Biobank bulk file, which includes a list of patient
    ids belonging to the respective data field id. A bulk file can be used as an input
    for UK Biobank's own utility script "ukbfetch" to download a subset of patient samples
    (i.e. imaging archives) from the UK Biobank. A bulkfile is a whitespace separated table
    of the following format (excluding the header):

    patient_id data_field_id instance_index array
    5585814 20209_2_0
    5162905 20209_2_0
    2305145 20209_2_0
    1125032 20209_2_0
    2474637 20209_2_0
    5416481 20209_2_0
    2567242 20209_2_0
    ...

    Parameters:
    -----------
    data_field : str
        The data field id for a specific UK Biobank bulk dataset. To see
        which data_field id points to which dataset,
        please refer to http://biobank.ndph.ox.ac.uk/showcase/list.cgi?it=20&vt=-1
    encoding_file : str
        The location of the decrypted UK Biobank encoding. This file can be downloaded from
        https://bbams.ndph.ox.ac.uk/ams/ on the showcase download page using a 32 character
        md5 checksum you should have received from the UK Biobank team via email.
    bulk_filename : str
        Path to output bulkfile (without extension).
    log : logical
        Flag to generate stdout and stderr logs (default: True)
    """

    if not encoding_file.endswith("_ukb"):
        print(["[ERROR] Are you sure encoding file is decrypted?"])

    ## for some reason ukbconv saves the .bulk file in the same directory
    ## in which the encoding file is located, so we will look for it in its directory
    ## and copy it over to output_dir
    encoding_dir = os.path.abspath(os.path.dirname(encoding_file))
    #    output_dir = os.path.abspath(os.path.dirname(output_file))

    ## the .bulk file will have the same name as the encoding file
    #    default_outfile = ".".join([os.path.basename(encoding_file).split(".")[0], "bulk"])
    #    print(os.path.join(encoding_dir, default_outfile))
    output_file_exists = os.path.isfile(bulk_filename + ".bulk")

    ## If a bulk file is found in the encoding directory
    ## move it to the expected output directory
    #    if default_outfile_exists:
    #        ensure_dir(output_file)
    #        shutil.move(os.path.join(encoding_dir, default_outfile), output_file)

    ## If there is not already a bulk file in our output directory
    ## we have to create one from the encoded ukb file ukbXXXXX.enc_ukb
    if not output_file_exists:

        cmd = [os.path.join(os.environ['UKBB_SCRIPTS_DIR'], "ukbconv"),
               encoding_file, "bulk", "-s%i" % data_field, "-o%s" % bulk_filename]
        print("[INFO] Calling command: %s" % " ".join(cmd))
        proc = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        out, err = proc.communicate()

        if log:
            if out:
                logfile = open("%s_stdout.log" % str(data_field), "a")
                for line in out.decode("utf8"):
                    sys.stdout.write(str(line))
                    logfile.write(str(line))
            if err:
                logfile = open("%s_stderr.log" % str(data_field), "a")
                for line in err.decode("utf8"):
                    sys.stdout.write(str(line))
                    logfile.write(str(line))
        proc.wait()

    else:
        print("[INFO] Bulkfile already exists. Skipping decryption of %s." % os.path.abspath(encoding_file))

    return (None)


def download_dataset(data_field, bulk_file, chunksize=500, log=True):
    """This function downloads a subset of the UK Biobank bulk data. Given a data field id
    pointing towards a specific bulk data set (i.e. 20209 - Short axis heart images - DICOM)
    it splits up the downloading process into batches. Be aware that the number of patients per
    batch should be limited to 1000 patient ids.

    Parameters:
    -----------
    data_field : str
        A data field id pointing towards a specific subset of the UK biobank bulk data
        To see which data_field id points to which dataset,
        please refer to http://biobank.ndph.ox.ac.uk/showcase/list.cgi?it=20&vt=-1
    bulk_file : str
        The location of the bulk file including the list of patient ids for the dataset
        refered to by the data field id. If you don't have a bulk file at hand the "create_bulk_file"
        function can create one for you, given a data field id.
    chunksize : int
        Batchsize of patient bulk data that is downloaded using UK Biobank's own utility script "ukbfetch"
        Be aware that UK Biobank prohibits ukbfetch calls which would download more than 1000 patients at once. (default: 500)
    log : logical
        Flag to generate stdout and stderr logs (default: True)
    """

    output_dir = os.path.abspath(args["output_dir"])
    bulk_file = bulk_file + ".bulk"

    ## Have some files already been downloaded?
    patient_files_exist = len(glob(os.path.join(output_dir, "*%s*.zip" % data_field))) > 0
    # print(patient_files_exist)
    if patient_files_exist:  ## if yes then which one's are still missing
        missing = get_missing_patients(bulk_file, output_dir)
        # print("%i left to download"%missing.shape[0])
        if (missing is not None):  # if there are anything missing remove already downloaded patients from bulkfile
            # print("%s will be updated"%bulk_file)
            data_frame_to_bulkfile(bulk_file, missing)
        else:
            raise ValueError("All patients have already been downloaded")

    n_patients = sum([not line.startswith("#") for line in open(bulk_file)])
    # n_last_chunk = n_patients % chunksize

    for i, j in enumerate(range(1, n_patients, chunksize)):
        cmd = [os.path.join(os.environ['UKBB_SCRIPTS_DIR'], "ukbfetch"), "-a%s" % os.environ['UKBB_AUTH_KEY'],
               "-b%s" % bulk_file,
               "-s%i" % j, "-m%i" % chunksize, "-o%s_fetched_%i" % (str(data_field), i)]
        print("[INFO] Calling command: %s" % " ".join(cmd))
        proc = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        out, err = proc.communicate()

        if log:
            if out:
                logfile = open("%s_stdout.log" % str(data_field), "a")
                for line in out.decode("utf8"):
                    sys.stdout.write(line)
                    logfile.write(line)
            if err:
                logfile = open("%s_stderr.log" % str(data_field), "a")
                for line in err.decode("utf8"):
                    sys.stdout.write(line)
                    logfile.write(line)

        if not os.path.exists(output_dir):
            os.makedirs(output_dir)

        patient_files = glob("*%s*.zip" % str(data_field))
        for f in patient_files:
            os.rename(os.path.abspath(f),
                      os.path.join(output_dir, f))

    return (None)


def parse_bulkfile(bulkfile):
    """
    This function parses a bulkfile created by "ukbconv" and returns a
    pandas data frame including patient_id, data_field_id, instance_index
    and array_index for each patient in the respective bulk.
    For further information on these ids see:
    https://biobank.ctsu.ox.ac.uk/crystal/exinfo.cgi?src=faq#columns

    Parameters:
    -----------
    bulk_file : str
        The location of the bulk file including the list of patient ids for the dataset
        refered to by the data field id. If you don't have a bulk file at hand the "create_bulk_file"
        function can create one for you, given a data field id.

    Returns:
    --------
    data_frame : pandas.DataFrame
        A dataframe including patient_id, data_field_id, instance_index and
        array_index for each patient in the respective bulk
    """

    data_frame = pd.read_csv(bulkfile, sep=" ", names=["patient_id", "data_field_id_combined"])
    data_field_id_df = pd.DataFrame(data_frame["data_field_id_combined"].str.split("_").tolist())
    data_frame = pd.concat([data_frame, data_field_id_df], axis=1)
    data_frame = data_frame.drop(columns=["data_field_id_combined"], axis=1)
    data_frame.columns = ["patient_id", "data_field_id", "instance_index", "array_index"]
    data_frame = data_frame.apply(pd.to_numeric)
    return (data_frame)


def parse_patient_filenames(directory, extension="zip"):
    """
    This function parses the filenames of each file with a user defined extension
    in a directory and returns a data frame including each sample id. Currently it
    assumes the files to be named according to UK Biobanks bulk file
    name convention <patient_id>_<data_field_id>_<instance_index>_<array_index>

    TODO: This is quite a rigid function and probably should be abstracted in the future.

    Parameters:
    -----------
    directory : str
        The path to the directory in which files should be parsed.
    extension: str
        The file extension of downloaded patient files (in case of imaging files this should be "zip")

    Returns:
    --------
    data_frame : pandas.DataFrame
        A dataframe including patient_id, data_field_id, instance_index and
        array_index for each patient in the respective bulk
    """

    patient_data_frame = None
    filenames = glob(os.path.join(directory, "*." + extension))
    if (len(filenames) > 0):
        patient_ids = list(map(lambda x: os.path.basename(x).split(".")[0].split("_"), filenames))
        patient_data_frame = pd.DataFrame(patient_ids)
        patient_data_frame.columns = ["patient_id", "data_field_id", "instance_index", "array_index"]
        patient_data_frame = patient_data_frame.apply(pd.to_numeric)
    return (patient_data_frame)


def get_missing_patients(bulk_file, directory):
    """
    This function compares the ids included in a bulk file with the patient ids indicated in the file
    names of downloaded files in a directory and returns a data frame including patient ids that were
    not downloaded. This data frame can then be easily converted to a new bulk file with which the
    missing patients can be downloaded.

    Parameters:
    -----------
    bulk_file : str
        The location of the bulk file including the list of patient ids for the dataset
        refered to by the data field id. If you don't have a bulk file at hand the "create_bulk_file"
        function can create one for you, given a data field id.
    directory: str
        The path to the download directory including patient files.

    Returns:
    --------
    data_frame : pandas.DataFrame
        A dataframe including patient_id, data_field_id, instance_index and
        array_index for each patient in the respective bulk
    """

    missing = None
    bulk_data_frame = parse_bulkfile(bulk_file)
    patient_data_frame = parse_patient_filenames(directory)

    if patient_data_frame is not None:
        downloaded = patient_data_frame.merge(bulk_data_frame,
                                              on=["patient_id", "data_field_id",
                                                  "instance_index", "array_index"])

        def _col_patient_instance(row):
            return f'{row["patient_id"]}_{row["instance_index"]}'

        # create a new column in both dataframes: PATIENT_ID + INSTANCE_ID
        # and filter missing based on this (as some patients might have multiple visits)
        downloaded['patient_id_instance'] = downloaded.apply(_col_patient_instance, axis=1)
        bulk_data_frame['patient_id_instance'] = bulk_data_frame.apply(_col_patient_instance, axis=1)

        missing = bulk_data_frame.loc[(~bulk_data_frame.patient_id_instance.isin(downloaded.patient_id_instance))]
    return missing


def data_frame_to_bulkfile(path, data_frame):
    """This function takes a data frame including patient_id, data_field_id, instance_index and
    array_index for each patient and converts it to a bulk file, that can be used as an input to
    "ukbfetch".
    """
    data_frame = data_frame.astype(str)
    data_frame["data_field_id_combined"] = data_frame[["data_field_id", "instance_index", "array_index"]].apply(
        lambda x: '_'.join(x), axis=1)
    data_frame = data_frame.drop(
        columns=["data_field_id", "instance_index", "array_index", 'patient_id_instance'],
        axis=1,
    )
    data_frame.to_csv(path, sep=" ", header=False, index=False)
    return (None)


def main():
    '''
    We start by checking whether the environment variables UKBB_AUTH_KEY and UKBB_SCRIPTS_DIR have been set.
    If that is the case we will create a bulk file for a dataset referenced to by its data field id. If
    a bulk file with the name
    '''

    check_env_vars()

    create_bulkfile(args["data_field"], args["encoding_file"], args["bulk_file"], log=True)
    if not args['no_download']:
        download_dataset(args["data_field"], bulk_file=args["bulk_file"], chunksize=500, log=True)

    clean_up()


if __name__ == "__main__":
    main()
