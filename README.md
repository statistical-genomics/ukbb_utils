# UKBB_Utils

## Description
This repository includes utility functions to access data from UK Biobank 
(hopefully) more conveniently from the command line

## Installation
1. Simply clone this repository to a location of your choice.
```sh
git clone https://gitlab.com/statistical-genomics/ukbb_utils.git
```
2. Make sure to export environment variables pointing towards a valid UK Biobank 
authentication key and the UK Biobank utility scripts directory.
```sh
export $UKBB_AUTH_KEY="$HOME/someKeyFile.key"
export $UKBB_SCRIPTS_DIR="$HOME/ukb_utils"
```

The UK Biobank scripts are currently provided in this repository, but you can 
always download an up-to-date version using 
[download_ukbb_scripts.py](download_ukbb_scripts.py)

## Dependencies
python >= 3.0

pandas >= 0.23.0

## Usage

```sh
usage: download_bulk_data.py [-h] -d DATA_FIELD -e ENCODING_FILE -b BULK_FILE
                             [-o OUTPUT_DIR]
```

## Credits
[Jens Hooge](mailto:Jens.Hooge@guest.hpi.de)
