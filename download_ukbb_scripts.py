#!/usr/bin/env python

## Author: Jens Hooge
## Date: Aug 7, 2019
## Python Version 3.6
##
## Please see http://biobank.ndph.ox.ac.uk/showcase/exinfo.cgi?src=accessing_data_guide#download
## for information on UKB specific scripts

import os
import shutil
import argparse
import subprocess

argparser = argparse.ArgumentParser()

argparser.add_argument("-o", "--output_dir",
                       action="store", type=str,
                       help="Output directory for downloaded ukb scripts",
                       required=True)

args = argparser.parse_args()
args = vars(args)

def ensure_dir(file_path):
    if not os.path.exists(file_path):
        os.makedirs(file_path)
    return(None)

def download_ukbb_scripts():
    """
    This function downloads all utility scripts from ukbiobank
    and tucks them away "output_dir". Make sure you make the
    scripts executable after they have been loaded.
    """
    scripts = ["ukbmd5", "ukbconv", "ukbunpack", "ukbfetch", "ukblink", "ukbgene"]
    cmd = ["wget -nd biobank.ndph.ox.ac.uk/showcase/util/ukbmd5; \
            wget -nd biobank.ndph.ox.ac.uk/showcase/util/ukbconv; \
            wget -nd biobank.ndph.ox.ac.uk/showcase/util/ukbunpack; \
            wget -nd biobank.ndph.ox.ac.uk/showcase/util/ukbfetch; \
            wget -nd biobank.ndph.ox.ac.uk/showcase/util/ukblink; \
            wget -nd biobank.ndph.ox.ac.uk/showcase/util/ukbgene"]
    proc = subprocess.call(cmd, shell=True)

    for script in scripts:
        shutil.move(os.path.abspath(script),
                    os.path.join(os.path.abspath(args["output_dir"]), script))



    return(None)

def main():
    ensure_dir(args["output_dir"])
    download_ukbb_scripts()

if __name__=="__main__":
    main()
