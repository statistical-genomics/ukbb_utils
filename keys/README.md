## README

Author: Jens Hooge
Date:  Aug 5, 2019

This folder includes the decryption keys for the ukbiobank 
encoding file.

All files should be named as follows:
<ApplicationID>____<ReleaseID>.key

Please check regularly for updates on the data at 
https://bbams.ndph.ox.ac.uk/ams/resProjects/details?appn_id=40502&activeTab=project_data

For each new release a new key can be downloaded from
https://biobank.ndph.ox.ac.uk/showcase/download.cgi?ams=39713d979ed2df1081195410af973a1a&tn=1565016685&ui=101510&ai=40502&bi=0

At the time of writing, the current key file is 40502_27451.key.
