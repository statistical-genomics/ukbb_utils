## README

The ukbiobank data of a respective release can be accessed via an encrypted dataset file, which include a on-to-many mapping from 
DataFieldIDs to their respective PatientIDs. To be able to download an up-to-date we have to enter an 34 character md5 checksum at

https://biobank.ndph.ox.ac.uk/showcase/download.cgi

Each file is named as follows
<ApplicationID>____<ReleaseID>

A new md5 checksum should be provided via email from the UK Biobank team after each update.
